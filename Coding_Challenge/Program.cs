﻿/*-----------------------------------------------------------------
Author:         Michael Koepp                   
Date:           4/26/2019
Namespace:      Coding_Challange for OneLogin
Class:          Program
Description:    Entry point for program.

                Write a command line program in the language of your choice that will take operations on fractions 
                as an input and produce a fractional result. Legal operators shall be *, /, +, - (multiply, divide, add, subtract)
                Operands and operators shall be separated by one or more spaces
                Mixed numbers will be represented by whole_numerator/denominator. e.g. "3_1/4"
                Improper fractions and whole numbers are also allowed as operands 

Revision History:
Name:           Date:        Description:
-----------------------------------------------------------------
*/

using System;
using System.Linq;

namespace Coding_Challenge
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter a two-fraction expression using the arithmetic operators +,-,*,/ (i.e. 2_3/8 + 9/8) -> ");
            string fractionalExpression = Console.ReadLine();

            FractionExpressionProcessor processor = new FractionExpressionProcessor();

            string resultAsMixedFraction = processor.ProcessExpression(fractionalExpression);

            Console.WriteLine("Result: " + resultAsMixedFraction);

            Console.ReadLine();
        }
    }
}
