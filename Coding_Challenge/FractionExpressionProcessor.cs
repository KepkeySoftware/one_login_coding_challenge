﻿/*-----------------------------------------------------------------
Author:         Michael Koepp                   
Date:           4/26/2019
Namespace:      Coding_Challange for OneLogin
Class:          FractionExpressionProcessor
Description:    Main class that performs arithmetic operations on
                expressions involving fractions,including proper,
                improper and mixed fractions.

Revision History:
Name:           Date:        Description:
-----------------------------------------------------------------
*/

using System;
using System.Linq;

namespace Coding_Challenge
{
    public class FractionExpressionProcessor
    {
        public string ProcessExpression(string expression)
        {
            char operation = GetOperator(expression);

            string[] operands = GetOperands(expression);

            string result = PerformArithmenticOperation(operands, operation);

            return result;
        }

        /// <summary>
        /// Parses the expression's arithmentic operator.
        /// </summary>
        /// <param name="expression">The fractional expression</param>
        /// <returns>Arithmentic operator</returns>
        private char GetOperator(string expression)
        {
            const int OPERATOR_INDEX = 1;

            string[] expressionTokens = expression.Split();

            return char.Parse(expressionTokens[OPERATOR_INDEX]);
        }

        /// <summary>
        /// Parses the expression's operands.
        /// </summary>
        /// <param name="expression">The fractional expression</param>
        /// <returns>Expression's operands.</returns>
        private string[] GetOperands(string expression)
        {
            const int OPERAND_COUNT = 2;
            const int EXPRESSION_LEFT_OPERAND_INDEX = 0;
            const int EXPRESSION_RIGHT_OPERAND_INDEX = 2;
            const int LEFT_OPERAND_INDEX = 0;
            const int RIGHT_OPERAND_INDEX = 1;

            string[] expressionTokens = expression.Split();

            string[] operands = new string[OPERAND_COUNT];

            string leftOperand = expressionTokens[EXPRESSION_LEFT_OPERAND_INDEX];
            string rightOperand = expressionTokens[EXPRESSION_RIGHT_OPERAND_INDEX];

            operands[LEFT_OPERAND_INDEX] = ConvertFromMixedFraction(leftOperand);
            operands[RIGHT_OPERAND_INDEX] = ConvertFromMixedFraction(rightOperand);

            return operands;
        }

        /// <summary>
        /// Carries out the arithmetic operation on the proper or improper fractions.
        /// </summary>
        /// <param name="operands">The proper or improper fractions.</param>
        /// <param name="operation">Arithmetic operation performed on the fractions.</param>
        /// <returns>The result of the arithmetic operation in proper or mixed form.</returns>
        private string PerformArithmenticOperation(string[] operands, char operation)
        {
            const int LEFT_OPERAND_INDEX = 0;
            const int RIGHT_OPERAND_INDEX = 1;

            string result = String.Empty;

            int leftOperandNumerator = GetNumerator(operands[LEFT_OPERAND_INDEX]);
            int leftOperandDenominator = GetDenominator(operands[LEFT_OPERAND_INDEX]);

            int rightOperandNumerator = GetNumerator(operands[RIGHT_OPERAND_INDEX]);
            int rightOperandDenominator = GetDenominator(operands[RIGHT_OPERAND_INDEX]);

            switch (operation)
            {
                case '+':
                    {
                        int commonDenominator = DetermineCommonDenominator(ref leftOperandNumerator, leftOperandDenominator, ref rightOperandNumerator, rightOperandDenominator);
                        int numerator = leftOperandNumerator + rightOperandNumerator;

                        result = BuildResultString(numerator, commonDenominator);

                        break;
                    }
                case '-':
                    {
                        int commonDenominator = DetermineCommonDenominator(ref leftOperandNumerator, leftOperandDenominator, ref rightOperandNumerator, rightOperandDenominator);
                        int numerator = leftOperandNumerator - rightOperandNumerator;

                        result = BuildResultString(numerator, commonDenominator);

                        break;
                    }
                case '*':
                    {
                        int numerator = leftOperandNumerator * rightOperandNumerator;
                        int denominator = leftOperandDenominator * rightOperandDenominator;

                        result = BuildResultString(numerator, denominator);

                        break;
                    }
                case '/':
                    {
                        int numerator = leftOperandNumerator * rightOperandDenominator;
                        int denominator = leftOperandDenominator * rightOperandNumerator;

                        result = BuildResultString(numerator, denominator);

                        break;
                    }
                default:
                    {
                        throw new Exception("Invalid operation. Take two aspirin and call in the morning.");
                    }
            }

            return result;
        }

        /// <summary>
        /// Puts operand in improper form if the operand is mixed, otherwise leaves it as 
        /// a proper fraction.
        /// </summary>
        /// <param name="operand">Proper, improper or mixed form.</param>
        /// <returns>Operand in either proper or improper form.</returns>
        private string ConvertFromMixedFraction(string operand)
        {
            const char MIXED_FRACTION_SEPARATOR = '_';
            const char FRACTION_NUMBER_SEPARATOR = '/';
            const int LEFT_PART_MIXED_FRACTION_INDEX = 0;
            const int RIGHT_PART_MIXED_FRACTION_INDEX = 1;
            const int RIGHT_TOKEN_NUMERATOR_INDEX = 0;
            const int RIGHT_TOKEN_DENOMINATOR_INDEX = 1;

            string fractionalForm = String.Empty;

            bool tokenExists = operand.Contains(MIXED_FRACTION_SEPARATOR);

            if (tokenExists)
            {
                string[] mixedFractionTokens = operand.Split(MIXED_FRACTION_SEPARATOR);

                string leftToken = mixedFractionTokens[LEFT_PART_MIXED_FRACTION_INDEX];
                string rightToken = mixedFractionTokens[RIGHT_PART_MIXED_FRACTION_INDEX];

                string[] numbersFromRightToken = rightToken.Split(FRACTION_NUMBER_SEPARATOR);

                int rightTokenNumerator = int.Parse(numbersFromRightToken[RIGHT_TOKEN_NUMERATOR_INDEX]);
                int rightTokenDenominator = int.Parse(numbersFromRightToken[RIGHT_TOKEN_DENOMINATOR_INDEX]);

                int wholePartOfMixedFraction = int.Parse(leftToken);

                int fractionalFormNumerator = (wholePartOfMixedFraction * rightTokenDenominator) + rightTokenNumerator;

                fractionalForm = fractionalFormNumerator.ToString() + FRACTION_NUMBER_SEPARATOR + rightTokenDenominator.ToString();
            }
            else
            {
                fractionalForm = operand;
            }

            return fractionalForm;
        }

        /// <summary>
        /// Formats the resultant expression in either proper or mixed form.
        /// </summary>
        /// <param name="numerator">The fraction's numerator.</param>
        /// <param name="denominator">The fraction's denominator.</param>
        /// <returns></returns>
        private string BuildResultString(int numerator, int denominator)
        {
            const char MIXED_FRACTION_SEPARATOR = '_';
            const char FRACTION_NUMBER_SEPARATOR = '/';

            string result = String.Empty;

            ReduceFractionToLowestTerms(ref numerator, ref denominator);

            int whole = numerator / denominator;

            int remainder = numerator % denominator;

            if (whole != 0 && remainder == 0)
            {
                result = whole.ToString();
            }
            else if (whole != 0 && remainder != 0)
            {
                result = whole.ToString() + MIXED_FRACTION_SEPARATOR + remainder.ToString() + FRACTION_NUMBER_SEPARATOR + denominator.ToString();
            }
            else if (whole == 0 && remainder != 0)
            {
                result = remainder.ToString() + FRACTION_NUMBER_SEPARATOR + denominator.ToString();
            }
            else if (whole == 0 && remainder == 0)
            {
                result = "0";
            }

            return result;
        }

        /// <summary>
        /// Get a common denominator if the fraction's denominators are different.
        /// This is only needed for fraction addition and subtraction operations.
        /// </summary>
        /// <param name="leftOperandNumerator"></param>
        /// <param name="leftOperandDenominator"></param>
        /// <param name="rightOperandNumerator"></param>
        /// <param name="rightOperandDenominator"></param>
        /// <returns>Common denominator</returns>
        private int DetermineCommonDenominator(ref int leftOperandNumerator, int leftOperandDenominator, ref int rightOperandNumerator, int rightOperandDenominator)
        {
            int commonDenominator = 0;

            if (leftOperandDenominator != rightOperandDenominator)
            {
                commonDenominator = leftOperandDenominator * rightOperandDenominator;

                leftOperandNumerator *= rightOperandDenominator;
                rightOperandNumerator *= leftOperandDenominator;
            }
            else commonDenominator = leftOperandDenominator; //Doesn't matter which denominator is used

            return commonDenominator;
        }

        /// <summary>
        /// Extracts the fractions numerator from a proper or improper fraction.
        /// </summary>
        /// <param name="operand"></param>
        /// <returns>Fraction's numerator</returns>
        private int GetNumerator(string operand)
        {
            const char FRACTION_NUMBER_SEPARATOR = '/';
            const int NUMERATOR_INDEX = 0;

            int numerator = 0;

            if (operand.Contains(FRACTION_NUMBER_SEPARATOR))
            {
                string[] tokens = operand.Split(FRACTION_NUMBER_SEPARATOR);

                numerator = int.Parse(tokens[NUMERATOR_INDEX]);
            }
            else numerator = int.Parse(operand);

            return numerator;
        }

        /// <summary>
        /// Extracts the fractions denominator from a proper or improper fraction.
        /// </summary>
        /// <param name="operand"></param>
        /// <returns>Fraction's denominator.</returns>
        private int GetDenominator(string operand)
        {
            const char FRACTION_NUMBER_SEPARATOR = '/';
            const int DENOMINATOR_INDEX = 1;

            int denominator = 0;

            if (operand.Contains(FRACTION_NUMBER_SEPARATOR))
            {
                string[] tokens = operand.Split(FRACTION_NUMBER_SEPARATOR);

                denominator = int.Parse(tokens[DENOMINATOR_INDEX]);
            }
            else denominator = int.Parse(operand);

            return denominator;
        }

        /// <summary>
        /// Determines the greatest common divisor and reduces the fraction to its lowest terms.
        /// </summary>
        /// <param name="numerator"></param>
        /// <param name="denominator"></param>
        private void ReduceFractionToLowestTerms(ref int numerator, ref int denominator)
        {
            int divisor = numerator;

            while (divisor > 0)
            {
                int numeratorRemainder = numerator % divisor;
                int denominatorRemainder = denominator % divisor;

                if (numeratorRemainder == 0 && denominatorRemainder == 0)
                {
                    numerator = numerator / divisor;
                    denominator = denominator / divisor;

                    return;
                }

                divisor--;
            }
        }
    }
}
