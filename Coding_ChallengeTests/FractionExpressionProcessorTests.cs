﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Coding_Challenge;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coding_Challenge.Tests
{
    [TestClass()]
    public class FractionExpressionProcessorTests
    {
        [TestMethod()]
        public void ProcessExpressionAdditionTest()
        {
            string result = String.Empty;

            FractionExpressionProcessor processor = new FractionExpressionProcessor();

            result = processor.ProcessExpression("1 + 1");

            Assert.AreEqual(result, "2");

            result = processor.ProcessExpression("1/2 + 1/2");

            Assert.AreEqual(result, "1");

            result = processor.ProcessExpression("1/2 + 2/3");

            Assert.AreEqual(result, "1_1/6");

            result = processor.ProcessExpression("1_1/2 + 1_2/3");

            Assert.AreEqual(result, "3_1/6");

            result = processor.ProcessExpression("2_3/8 + 9/8");

            Assert.AreEqual(result, "3_1/2");
        }

        [TestMethod()]
        public void ProcessExpressionSubtractionTest()
        {
            string result = String.Empty;

            FractionExpressionProcessor processor = new FractionExpressionProcessor();

            result = processor.ProcessExpression("1 - 1");

            Assert.AreEqual(result, "0");

            result = processor.ProcessExpression("1/2 - 1/2");

            Assert.AreEqual(result, "0");

            result = processor.ProcessExpression("1/2 - 2/3");

            Assert.AreEqual(result, "-1/6");

            result = processor.ProcessExpression("1_1/2 - 1_2/3");

            Assert.AreEqual(result, "-1/6");

            result = processor.ProcessExpression("2_3/8 - 9/8");

            Assert.AreEqual(result, "1_1/4");
        }

        [TestMethod()]
        public void ProcessExpressionMultiplicationTest()
        {
            string result = String.Empty;

            FractionExpressionProcessor processor = new FractionExpressionProcessor();

            result = processor.ProcessExpression("1 * 1");

            Assert.AreEqual(result, "1");

            result = processor.ProcessExpression("1/2 * 1/2");

            Assert.AreEqual(result, "1/4");

            result = processor.ProcessExpression("1/2 * 3_3/4");

            Assert.AreEqual(result, "1_7/8");
        }

        [TestMethod()]
        public void ProcessExpressionDivisionTest()
        {
            string result = String.Empty;

            FractionExpressionProcessor processor = new FractionExpressionProcessor();

            result = processor.ProcessExpression("1 / 1");

            Assert.AreEqual(result, "1");

            result = processor.ProcessExpression("1/2 / 1/2");

            Assert.AreEqual(result, "1");

            result = processor.ProcessExpression("1/2 / 3_3/4");

            Assert.AreEqual(result, "2/15");
        }
    }
}